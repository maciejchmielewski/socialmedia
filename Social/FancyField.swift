//
//  FancyField.swift
//  Social
//
//  Created by Maciej Chmielewski on 06.04.2017.
//  Copyright © 2017 Maciej Chmielewski. All rights reserved.
//

import UIKit

@IBDesignable class FancyField: UITextField {

  override func layoutSubviews() {
    super.layoutSubviews()
    layer.borderColor = UIColor.gray.withAlphaComponent(0.2).cgColor
    layer.borderWidth = 1
    layer.cornerRadius = 2
  }
  
  override func textRect(forBounds bounds: CGRect) -> CGRect {
    return bounds.insetBy(dx: 10, dy: 5)
  }
  
  override func editingRect(forBounds bounds: CGRect) -> CGRect {
    return bounds.insetBy(dx: 10, dy: 5)
  }
}





