//
//  FancyButton.swift
//  Social
//
//  Created by Maciej Chmielewski on 06.04.2017.
//  Copyright © 2017 Maciej Chmielewski. All rights reserved.
//

import UIKit

class FancyButton: UIButton {
  
  override func layoutSubviews() {
    super.layoutSubviews()
    layer.shadowColor = UIColor.gray.cgColor
    layer.shadowOffset = CGSize(width: 0, height: 2)
    layer.shadowRadius = 5.0
    layer.shadowOpacity = 0.8
    layer.cornerRadius = 2.0
  }
}
