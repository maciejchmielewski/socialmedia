//
//  File.swift
//  Social
//
//  Created by Maciej Chmielewski on 07.04.2017.
//  Copyright © 2017 Maciej Chmielewski. All rights reserved.
//

import Foundation
import Firebase
import SwiftKeychainWrapper

let DB_BASE = Database.database().reference()
let STORAGE_BASE = Storage.storage().reference()

class DataService {
  
  //Singleton for DataService
  static let ds = DataService()
  
  //Private API
  private var _REF_BASE = DB_BASE
  private var _REF_POSTS =  DB_BASE.child("posts")
  private var _REF_USERS = DB_BASE.child("users")
  private var _REF_POSTS_PICS = STORAGE_BASE.child("post_pics")
  private var _REF_AVATAR_PICS = STORAGE_BASE.child("avatar_pics")
  
  
  //Public API
  var REF_BASE: DatabaseReference {
    return _REF_BASE
  }
  
  var REF_POSTS: DatabaseReference {
    return _REF_POSTS
  }
  
  var REF_USERS: DatabaseReference {
    return _REF_USERS
  }
  
  var REF_USER_CURRENT: DatabaseReference {
    //Getting current user`s UID from keyChain
    let uid = KeychainWrapper.standard.string(forKey: "uid")
    let user = REF_USERS.child(uid!)
    return user
  }
  
  var REF_POST_IMAGE: StorageReference {
    return _REF_POSTS_PICS
  }
  
  var REF_AVATAR_PICS: StorageReference {
    return _REF_AVATAR_PICS
  }
  
  func createFirebaseDBUser(uid: String, userData: [String: String]){
    REF_USERS.child(uid).updateChildValues(userData)
  }
}






