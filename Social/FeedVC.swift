//
//  FeedVC.swift
//  Social
//
//  Created by Maciej Chmielewski on 07.04.2017.
//  Copyright © 2017 Maciej Chmielewski. All rights reserved.
//

import UIKit
import Firebase

//Protocol for removing credential from keyChain while signing out.
protocol SignOutDelegate {
  func signOut()
}

class FeedVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var addImage: RoundImageView!
  @IBOutlet weak var captionField: FancyField!
  
  private var posts = [Post]()
  private var imagePicker: UIImagePickerController!
  private var imageSelected = false
  private var avatarImage: UIImage?
  private var nickname: String?
  private var avatarImageUrl: String?
  private var avatars = [String: UIImage]()
  private var postImages = [String: UIImage]()
  
  var signOutDelegate: SignOutDelegate?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    getName()
    getAvatarImageUrl()
    tableView.delegate = self
    tableView.dataSource = self
    imagePicker = UIImagePickerController()
    imagePicker.allowsEditing = false
    imagePicker.delegate = self
    //Downloading posts from Firebase
    DataService.ds.REF_POSTS.observe(.value, with: { (snapshot) in
      if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
        for snap in snapshots {
          if let postsDict = snap.value as? [String: AnyObject] {
            let key = snap.key
            if self.avatars.keys.contains(key) {
              for i in 0..<self.posts.count {
                if self.posts[i].postKey == key {
                  self.posts.remove(at: i)
                  let post = Post(postKey: key, postData: postsDict)
                  self.posts.insert(post, at: i)
                }
              }
            } else {
              self.downloadAvatar(url: postsDict["avatarImageUrl"] as! String, key: key)
              self.downloadPostImage(url: postsDict["imageUrl"] as! String, key: key)
              let post = Post(postKey: key, postData: postsDict)
              self.posts.append(post)
            }
          }
        }
      }
      self.tableView.reloadData()
    })
    captionField.delegate = self
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
      addImage.image = image
      imageSelected = true
    }
    imagePicker.dismiss(animated: true, completion: nil)
    self.reloadInputViews()
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let post = posts[indexPath.row]
    let key = post.postKey
    let avatar = avatars[key]
    let postImage = postImages[key]
    if let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell") as? PostCell {
      cell.configureCell(post: post, avatarImage: avatar!,postImage: postImage!)
      return cell
    } else {
      return PostCell()
    }
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return posts.count
  }
  
  @IBAction func postBtnTapped(_ sender: RoundButton) {
    guard let caption = captionField.text, caption != "" else { return }
    guard let img = addImage.image, imageSelected else { return }
    if let imgData = UIImageJPEGRepresentation(img, 0.2) {
      let imageuid = NSUUID().uuidString
      let metadata = StorageMetadata()
      metadata.contentType = "image/jpeg"
      //Post image to Firebase storage
      DataService.ds.REF_POST_IMAGE.child(imageuid).putData(imgData, metadata: metadata) { (metadata, error) in
        if error != nil{
          print("Cannot upload data to firebase storage")
        } else {
          let downloadUrl = metadata?.downloadURL()?.absoluteString
          self.postToFirebase(imageUrl: downloadUrl!)
        }
      }
    }
  }
  
  func postToFirebase(imageUrl: String) {
    let post: [String: Any] = [
      "caption": captionField.text!,
      "nickname": nickname!,
      "imageUrl": imageUrl,
      "avatarImageUrl": avatarImageUrl!,
      "likes": 0
    ]
    
    //Creating new post
    let firebasePost = DataService.ds.REF_POSTS.childByAutoId()
    firebasePost.setValue(post)
    
    captionField.text = ""
    addImage.image = UIImage(named: "add-image")
    imageSelected = false
    
    tableView.reloadData()
  }
  
  @IBAction func signOutTapped(_ sender: Any) {
    if let delegate = signOutDelegate {
      delegate.signOut()
      dismiss(animated: true, completion: nil)
    }
  }
  
  private func getAvatarImageUrl() {
    DataService.ds.REF_USER_CURRENT.child("avatarImageUrl").observeSingleEvent(of: .value, with: { (snapshot) in
      let avatarImageUrl = snapshot.value as? String
      self.avatarImageUrl = avatarImageUrl
    }) { (error) in
      print(error.localizedDescription)
    }
  }
  
  private func getName() {
    DataService.ds.REF_USER_CURRENT.child("nickname").observeSingleEvent(of: .value, with: { (snapshot) in
      let nickname = snapshot.value as? String
      self.nickname = nickname
    }) { (error) in
      print(error.localizedDescription)
    }
  }
  
  private func downloadAvatar(url: String, key: String) {
    guard let avatarURL = URL(string: url) else { return }
    guard let avatarData = try? Data(contentsOf: avatarURL) else { return }
    guard let avatarImage = UIImage(data: avatarData) else { return }
    avatars[key] = avatarImage
  }
  
  private func downloadPostImage(url: String, key: String) {
    guard let postImageURL = URL(string: url) else { return }
    guard let postImageData = try? Data(contentsOf: postImageURL) else { return }
    guard let postImage = UIImage(data: postImageData) else { return }
    postImages[key] = postImage
  }
  
  @IBAction func addImageTapped(_ sender: Any) {
    present(imagePicker, animated: true, completion: nil)
  }
}
