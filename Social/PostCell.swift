//
//  PostCell.swift
//  Social
//
//  Created by Maciej Chmielewski on 07.04.2017.
//  Copyright © 2017 Maciej Chmielewski. All rights reserved.
//

import UIKit
import Firebase

class PostCell: UITableViewCell {
  
  @IBOutlet weak var profileImg: RoundImageView!
  @IBOutlet weak var userLbl: UILabel!
  @IBOutlet weak var postImg: UIImageView!
  @IBOutlet weak var caption: UITextView!
  @IBOutlet weak var likesLbl: UILabel!
  @IBOutlet weak var likeImg: UIImageView!
  
  private var post: Post!
  private var likesRef: DatabaseReference!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    let tap = UITapGestureRecognizer(target: self, action: #selector(likeTapped))
    likeImg.addGestureRecognizer(tap)
    likeImg.isUserInteractionEnabled = true
  }
  
  func configureCell(post: Post, avatarImage: UIImage, postImage: UIImage){
    self.post = post
    likesRef = DataService.ds.REF_USER_CURRENT.child("likes").child(post.postKey)
    self.caption.text = post.caption
    self.userLbl.text = post.nickname
    self.likesLbl.text = "\(post.likes)"
    self.postImg.image = postImage
    likesRef.observeSingleEvent(of: .value, with: { (snapshot) in
      if let _ = snapshot.value as? NSNull {
        //If current user does not like the post yet
        self.likeImg.image = UIImage(named: "empty-heart")
      } else {
        self.likeImg.image = UIImage(named: "filled-heart")
      }
    })
    profileImg.image = avatarImage
  }
  
  func likeTapped(sender: UITapGestureRecognizer) {
    likesRef.observeSingleEvent(of: .value, with: { (snapshot) in
      if let _ = snapshot.value as? NSNull {
        self.likeImg.image = UIImage(named: "filled-heart")
        self.post.adjustLikes(addLike: true)
        self.likesRef.setValue(true)
      } else {
        //If current user liked the post before
        self.likeImg.image = UIImage(named: "empty-heart")
        self.post.adjustLikes(addLike: false)
        self.likesRef.removeValue()
      }
    })
  }
}




