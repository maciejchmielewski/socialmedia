//
//  Post.swift
//  Social
//
//  Created by Maciej Chmielewski on 09.04.2017.
//  Copyright © 2017 Maciej Chmielewski. All rights reserved.
//

import Foundation
import Firebase

class Post {
  
  //Private API
  private var _caption: String!
  private var _nickname: String!
  private var _imageUrl: String!
  private var _avatarImageUrl: String!
  private var _likes: Int!
  private var _postKey: String!
  private var _postRef: DatabaseReference!
  
  //Public API
  var caption: String {
    return _caption
  }
  
  var nickname: String {
    return _nickname
  }
  
  var imageUrl: String {
    return _imageUrl
  }
  
  var avatarImageUrl: String {
    return _avatarImageUrl
  }
  
  var likes: Int {
    return _likes
  }
  
  var postKey: String {
    return _postKey
  }
  
  init(postKey: String, postData: [String: AnyObject]) {
    _postKey = postKey
    if let caption = postData["caption"] as? String {
      _caption = caption
    }
    
    if let nickname = postData["nickname"] as? String {
      _nickname = nickname
    }
    
    if let imageUrl = postData["imageUrl"] as? String {
      _imageUrl = imageUrl
    }
    
    if let avatarImageUrl = postData["avatarImageUrl"] as? String {
      _avatarImageUrl = avatarImageUrl
    }
    
    if let likes = postData["likes"] as? Int {
      _likes = likes
    }
    
    _postRef = DataService.ds.REF_POSTS.child(_postKey)
  }
  
  func adjustLikes(addLike: Bool) {
    if addLike {
      _likes = _likes + 1
    } else {
      _likes = _likes - 1
    }
    _postRef.child("likes").setValue(likes)
  }
}
