//
//  RegisterVC.swift
//  Social
//
//  Created by Maciej Chmielewski on 09.05.2017.
//  Copyright © 2017 Maciej Chmielewski. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import Firebase

class RegisterVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  
  @IBOutlet weak var emailField: FancyField!
  @IBOutlet weak var nickField: FancyField!
  @IBOutlet weak var passwordField: FancyField!
  @IBOutlet weak var repeatPasswordField: FancyField!
  @IBOutlet weak var avatarImage: RoundImageView!
  
  private let imagePicker = UIImagePickerController()
  //Degault avatar URL
  private var avatarUrl = "https://firebasestorage.googleapis.com/v0/b/social-f1a74.appspot.com/o/avatar_pics%2FemptyAvatar.jpg?alt=media&token=a394d814-e0d3-40c3-a727-0a04e66be5e1"
  
  override func viewDidLoad() {
    super.viewDidLoad()
    imagePicker.delegate = self
    imagePicker.allowsEditing = true
  }
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
      avatarImage.image = image
      postAvatarImage(image)
      dismiss(animated: true, completion: nil)
    }
  }
  
  private func postAvatarImage(_ image: UIImage) {
    if let imageData = UIImageJPEGRepresentation(image, 0.2) {
      let imageUid = NSUUID().uuidString
      let metaData = StorageMetadata()
      metaData.contentType = "image/jpeg"
      //Posting avatar image to firebase storage
      DataService.ds.REF_AVATAR_PICS.child(imageUid).putData(imageData, metadata: metaData) { (metadata, error) in
        if error != nil {
          print("Cannot upload avatar data to firebase storage")
        } else {
          if let downloadUrl = metadata?.downloadURL()?.absoluteString {
            self.avatarUrl = downloadUrl
          }
        }
      }
    }
  }
  
  @IBAction func backBtnPrsd(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }
  
  @IBAction func doneBtnPrsd(_ sender: Any) {
    //Checking if all of the fields are filled
    if !(emailField.text?.isEmpty)! && !(nickField.text?.isEmpty)! && !(passwordField.text?.isEmpty)! && !(repeatPasswordField.text?.isEmpty)! {
      if passwordField.text != repeatPasswordField.text {
        let alert = UIAlertController(title: "Error", message: "Password fields does not match", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
      } else {
        Auth.auth().createUser(withEmail: emailField.text!, password: passwordField.text!) { (user, error) in
          if let error = error {
            var errorString = String(describing: error)
            errorString = errorString.components(separatedBy: "\"")[1]
            let alert = UIAlertController(title: "Error", message: errorString, preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
          } else if let user = user {
            //Posting new user`s data to firebase database
            let userData = ["nickname": self.nickField.text!, "avatarImageUrl": self.avatarUrl]
            DataService.ds.createFirebaseDBUser(uid: user.uid, userData: userData)
            self.dismiss(animated: true, completion: nil)
          }
        }
      }
    } else {
      let alert = UIAlertController(title: "Error", message: "Fill in empty fields", preferredStyle: .alert)
      let action = UIAlertAction(title: "OK", style: .default, handler: nil)
      alert.addAction(action)
      present(alert, animated: true, completion: nil)
    }
  }
  
  @IBAction func avatarBtnPrsd(_ sender: Any) {
    present(imagePicker, animated: true, completion: nil)
  }
}
