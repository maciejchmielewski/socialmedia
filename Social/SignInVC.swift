//
//  ViewController.swift
//  Social
//
//  Created by Maciej Chmielewski on 06.04.2017.
//  Copyright © 2017 Maciej Chmielewski. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import Firebase
import SwiftKeychainWrapper

class SignInVC: UIViewController, SignOutDelegate, UITextFieldDelegate {
  
  @IBOutlet weak var emailField: FancyField!
  @IBOutlet weak var passwordField: FancyField!
  
  private var userData: [String: String]!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    emailField.delegate = self
    passwordField.delegate = self
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
  
  override func viewDidAppear(_ animated: Bool) {
    //Check if there is a password in a keyChain and eventually sign in
    if let _ = KeychainWrapper.standard.string(forKey: "uid") {
      performSegue(withIdentifier: "GoToFeed", sender: nil)
    }
  }
  
  private func firebaseAuth(_ credential: AuthCredential) {
    Auth.auth().signIn(with: credential) { (user, error) in
      if error != nil {
        print("Unable to authenticate to firebase!")
      } else {
        if let user = user {
          DataService.ds.createFirebaseDBUser(uid: user.uid, userData: self.userData)
          self.completeSignIn(id: user.uid)
        }
      }
    }
  }
  
  private func downloadAvatar(url: String) {
    guard let avatarURL = URL(string: url) else { return }
    guard let avatar = try? Data(contentsOf: avatarURL) else { return }
    guard let avatarImage = UIImage(data: avatar) else { return }
    postAvatarImage(image: avatarImage)
  }
  
  func signOut() {
    _ = KeychainWrapper.standard.removeAllKeys()
    emailField.text?.removeAll()
    passwordField.text?.removeAll()
  }
  
  private func postAvatarImage(image: UIImage) {
    if let imgData = UIImageJPEGRepresentation(image, 0.2) {
      let imageuid = NSUUID().uuidString
      let metaData = StorageMetadata()
      metaData.contentType = "image/jpeg"
      DataService.ds.REF_AVATAR_PICS.child(imageuid).putData(imgData, metadata: metaData) { (metadata, error) in
        if error != nil{
          print("Cannot upload avatar data to firebase storage")
        }
      }
    }
  }
  
  private func completeSignIn(id: String) {
    _ = KeychainWrapper.standard.set(id, forKey: "uid")
    performSegue(withIdentifier: "GoToFeed", sender: nil)
  }
  
  @IBAction func facebookButtonPressed(_ sender: Any) {
    let facebookLogIn = FBSDKLoginManager()
    facebookLogIn.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
      if error != nil {
        print("Unable to authenticate to Facebook")
      } else {
        //Get users first name and id from facebook
        let request = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "first_name, id"])
        request?.start { (_, result, error) in
          if error != nil {
            print(error!)
          }
          if let results = result as? [String: String] {
            let userID = results["id"]
            let firstName = results["first_name"]
            //Gemerate URL to facebook profile image
            let profileImageUrl = "http://graph.facebook.com/\(userID!)/picture?type=large"
            self.userData = ["nickname": firstName!, "avatarImageUrl": profileImageUrl]
            self.downloadAvatar(url: profileImageUrl)
          }
        }
        let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
        self.firebaseAuth(credential)
      }
    }
  }
  
  @IBAction func signInTapped(_ sender: UIButton) {
    if let email = emailField.text, let password = passwordField.text {
      Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
        if error == nil {
          if let user = user{
            self.completeSignIn(id: user.uid)
          }
        }
      }
    }
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if let destination = segue.destination as? FeedVC{
      destination.signOutDelegate = self
    }
  }
}

